
PIC32 Connections- 

4 Digital Output pins M1IN1, M1IN2, M2IN1, M2IN2 [pins 70, 71, 32, 33]
4 Digital Input pins M1 encoder A & B, M2 encoder A & B [pins 10, 11, 12, 14]
4 Interrupts  Change Notification for encoders [CN 8, 9, 10, 11]
2 Output Compares  M1 pwm, M2 pwm [OC1, OC3]
1 Analog Input pin Distance Sensor [AN12]
I2C5 via Raspberry Pi SDA5, SCL5 [pins 49 and 50]

H-bridge Connections for Motor 1 and 2-  

5V (PIC32) EN, SLEW,  
GND(PIC32) GRD, M1 PWM D1, M2 PWM D1 
PWM(PIC32) M1 PWM D2, M2 PWM D2

Raspberry PI Connection-

SDA SCL connects to SDA5, SCL5 [pins 49 and 50] on PIC32
GND also connects to GND on PIC32

For Raspberry please download the turtle graphics library if not installed and enable i2c on the SMBus