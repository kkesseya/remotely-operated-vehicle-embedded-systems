// pin 72 for pwm output 1 
// pin 77 for pwm output 3
// pin 70 and 71 connect to m1in1 and m1in2
// pin 32 and 33 connect to m2in1 and m2in2
// pin 10 and 11 connect to encoder A and B from the motor 1
// pin 12 and 14 connect to encoder A and B from the motor 2
// pin 41 connect to distance sensor

#include <xc.h> // PIC32 header file
#include <sys/attribs.h> // sys header file
#include <string.h> // header file to use strlen
#include <stdio.h>  // header file to use sprintf
#include "display.h" // header file containing main functions
#include "i2c_slave.h" // header file or i2c and motor control functions

// parameters 
#pragma config DEBUG = OFF          // Background Debugger disabled
#pragma config FWDTEN = OFF         // WD timer: OFF
#pragma config WDTPS = PS4096       // WD period: 4.096 sec
#pragma config POSCMOD = HS         // Primary Oscillator Mode: High Speed crystal
#pragma config FNOSC = PRIPLL       // Oscillator Selection: Primary oscillator w/ PLL
#pragma config FPLLMUL = MUL_20     // PLL Multiplier: Multiply by 20
#pragma config FPLLIDIV = DIV_2     // PLL Input Divider:  Divide by 2
#pragma config FPLLODIV = DIV_1     // PLL Output Divider: Divide by 1
#pragma config FPBDIV = DIV_1       // Peripheral Bus Clock: Divide by 1
#pragma config UPLLEN = ON          // USB clock uses PLL
#pragma config UPLLIDIV = DIV_2     // Divide 8 MHz input by 2, mult by 12 for 48 MHz
#pragma config FUSBIDIO = ON        // USBID controlled by USB peripheral when it is on
#pragma config FVBUSONIO = ON       // VBUSON controlled by USB peripheral when it is on
#pragma config FSOSCEN = OFF        // Disable second osc to get pins back
#pragma config BWP = ON             // Boot flash write protect: ON
#pragma config ICESEL = ICS_PGx2    // ICE pins configured on PGx2
#pragma config FCANIO = OFF         // Use alternate CAN pins
#pragma config FMIIEN = OFF         // Use RMII (not MII) for ethernet
#pragma config FSRSSEL = PRIORITY_6 // Shadow Register Set for interrupt priority 6

#define REVOLUTION_COUNT 4741.44 // motor revolution count
#define MAX 100.00 // max controller output
#define MIN 0.00  // min controller output
volatile unsigned int oldB1 = 0, oldA1 = 0, newB1 = 0, newA1 =0; // motor 1 encoder variables
volatile unsigned int oldB2 = 0, oldA2 = 0, newB2 = 0, newA2 =0; // motor 2 encoder variables
volatile int Counter1 = 0, Counter2 = 0; // encoder count 
volatile float degree1 = 0, degree2 = 0; // angle of motor
volatile float Kp = 20; // P controller variables
volatile float reference1 = 0, reference2 =0; // angle parameters
float integral_1 = 0, error1 = 0, Poutput1 = 0, integral_2 = 0, error2 = 0, Poutput2 = 0; // P controller variables
float maxPeriod = 190, minPeriod = 70; // max and min duty cycle

// state machine logic for motor 1 to get encoder count
void updateCounter1(unsigned int newA,unsigned int newB, unsigned int oldA, unsigned int oldB){
    switch(oldA){//old encoder A
        case 1:
            switch(oldB){//old encoder B
                case 1://11
                    if((newA == 1) && (newB == 0)){//11->10
                        //increment counter
                        Counter1++;
                        return;
                    }else if((newA == 0) && (newB == 1)){//11 -> 01
                        //decrement counter
                        Counter1--;
                        return;
                    }    
                    break;
                case 0://10
                    if((newA == 0) && (newB == 0)){//10->00
                        //increment counter
                        Counter1++;
                        return;
                    }else if((newA == 1) && (newB == 1)){//10 -> 11
                        //decrement counter
                       Counter1--;
                       return;
                    }    
                    break;
            }
            break;
        case 0:
            switch(oldB){
                case 1://01
                    if((newA == 1) && (newB == 1)){//01->11
                        //increment counter
                        Counter1++;
                        return;
                    }else if((newA == 0) && (newB == 0)){//01 -> 00
                        //decrement counter
                        Counter1--;
                        return;
                    }    
                    break;
                case 0://00
                    if((newA == 0) && (newB == 1)){//00->01
                        //increment counter
                        Counter1++;
                        return;
                    }else if((newA == 1) && (newB == 0)){//00 -> 10
                        //decrement counter
                        Counter1--;
                        return;
                    }    
                    break;
            }
            break;
    }   
}

// state machine logic for motor 2 to get encoder count
void updateCounter2(unsigned int newA,unsigned int newB, unsigned int oldA, unsigned int oldB){
    switch(oldA){//old encoder A
        case 1:
            switch(oldB){//old encoder B
                case 1://11
                    if((newA == 1) && (newB == 0)){//11->10
                        //increment counter
                        Counter2++;
                        return;
                    }else if((newA == 0) && (newB == 1)){//11 -> 01
                        //decrement counter
                        Counter2--;
                        return;
                    }    
                    break;
                case 0://10
                    if((newA == 0) && (newB == 0)){//10->00
                        //increment counter
                        Counter2++;
                        return;
                    }else if((newA == 1) && (newB == 1)){//10 -> 11
                        //decrement counter
                       Counter2--;
                       return;
                    }    
                    break;
            }
            break;
        case 0:
            switch(oldB){
                case 1://01
                    if((newA == 1) && (newB == 1)){//01->11
                        //increment counter
                        Counter2++;
                        return;
                    }else if((newA == 0) && (newB == 0)){//01 -> 00
                        //decrement counter
                        Counter2--;
                        return;
                    }    
                    break;
                case 0://00
                    if((newA == 0) && (newB == 1)){//00->01
                        //increment counter
                        Counter2++;
                        return;
                    }else if((newA == 1) && (newB == 0)){//00 -> 10
                        //decrement counter
                        Counter2--;
                        return;
                    }    
                break;
            }
        break;
    }   
} 

// function to convert P output to duty cycle value 
float minMax(float origMin, float origMax, float newMin, float newMax, float origValue){
    // scale P output to fit duty cycle range
    return ((((origValue - origMin)/(origMax-origMin)) * (newMax-newMin)) + newMin); 
}

// Proportional controller for speed of motor 1
float control_1(float reference, float angle) {
    //Calculate P
    float output; // temp value to store P output
    error1 = reference - angle; // get error value
    output = Kp*error1; // calculate P output
    output = abs(output); // get absolute value 
    
    //Saturation Filter to cap the output values
    if (output >= MAX) { // if greater than 100
        output = MAX;
    } else if (output < MIN) { // if less than 0
        output = MIN;
    }
    
    // convert raw value to duty cycle 
    output = minMax(MIN,MAX,minPeriod,maxPeriod,output);
    return output;
}
// Proportional controller for speed of motor 2
float control_2(float reference, float angle) {
    //Calculate P
    float output; // temp value to store P output
    error2 = reference - angle; // get error value
    output = Kp*error2; // calculate P output
    output = abs(output); // get absolute value 
    
    //Saturation Filter to cap the output values
    if (output >= MAX) { // if greater than 100
        output = MAX;
    } else if (output < MIN) { // if less than 0
        output = MIN;
    }
    
    // convert raw value to duty cycle 
    output = minMax(MIN,MAX,minPeriod,maxPeriod,output);
    return output;
}

// analog to digital converter for distance sensor           
unsigned int adc_sample_convert(int pin){
    int elasped, finish = 0;
    AD1CHSbits.CH0SA = pin; // connect pin 12 to MUXA for sampling
    AD1CON1bits.SAMP = 1; // start sampling
    elasped = _CP0_GET_COUNT();
    finish = elasped + 10;
    while(_CP0_GET_COUNT() < finish){
        ; // sample for more than 250 ns
    }
    // sample
    AD1CON1bits.SAMP = 0; // stop sampling and start converting
    while(!AD1CON1bits.DONE){
        ; // wait for the conversion process to finish
    }
    return ADC1BUF0; // return digital value
}

int sample = 0; // store adc value
//return 1 if the vehicle is blocked, return 0 if the vehicle is not blocked
int isBlocked(){
    sample = adc_sample_convert(12); // get distance sensor data
    if(sample >= 900){  // 900 is close enough range ~10cm
        if(getForward()){   // determine if in forward motion
            moveNull(); // if yes, stop motors
        }
        return 1;
    }
    return 0;
}

// encoder value change notification ISR
void __ISR(_CHANGE_NOTICE_VECTOR, IPL5SOFT) CNISR(void){
        // read motor 1 encoder values
        newB1 = PORTGbits.RG6; // pin 10 read encoder B and update value
        newA1 = PORTGbits.RG7; // pin 11 read encoder A and update value      
        updateCounter1(newA1, newB1, oldA1, oldB1); // update encoder count
        oldB1 = newB1; // save old encoder B value
        oldA1 = newA1; // save old encoder A value

        // read motor 2 encoder values
        newB2 = PORTGbits.RG8; // pin 12 read encoder B and update value
        newA2 = PORTGbits.RG9; // pin 14 read encoder A and update value      
        updateCounter2(newA2, newB2, oldA2, oldB2); // update encoder count
        oldB2 = newB2; // save old encoder B value
        oldA2 = newA2; // save old encoder A value

        if((getForward() == 0)&(getBackward() == 0)){
            Counter1 = 0;
            Counter2 = 0;
        }
        
        IFS1bits.CNIF = 0; //clear the interupt flag          
}

// get angle of motor 1
int getDegree1(){
    // calculate angle using encoder counts
    degree1 = (Counter1*360.00)/REVOLUTION_COUNT;
    return degree1;
}
// get angle of motor 2
int getDegree2(){
    // calculate angle using encoder counts
    degree2 = (Counter2*360.00)/REVOLUTION_COUNT;
    return degree2;
}

int main(void){
    LATA = 0x00;
    DDPCON = 0; // set DDPCON register to 0
    TRISA = 0; // set TRISA registers to 0
    TRISBbits.TRISB15 = 0; // RS TRIS register to 0
    TRISDbits.TRISD4 = 0; // ENABLE register to 0
    TRISDbits.TRISD5 = 0; // RW register to 0
    // CHANGE NOTIFS
    TRISGbits.TRISG6 = 1; // CN 8 INPUT
    TRISGbits.TRISG7 = 1; // CN 9 INPUT
    TRISGbits.TRISG8 = 1; // CN 10 INPUT
    TRISGbits.TRISG9 = 1; // CN 11 INPUT
    // DRIVE motor FORWARD/BACKWARD
    TRISBbits.TRISB8 = 0; // PIN 32 output m2
    TRISBbits.TRISB9 = 0; // PIN 33 output m2
    TRISDbits.TRISD10 = 0; // PIN 70 output m1
    TRISDbits.TRISD11 = 0; // PIN 71 output m1
    // INITIALIZE DISPLAY
    TRISE = 0; // set LCD bits to 0 for output
    display_driver_initialize(); // initialise display
    display_driver_clear(); // clear display    
    
    // clear notification interrupts to get encoder values
    __builtin_disable_interrupts();
    INTCONbits.MVEC = 0x1; // multi vector mode
    i2c_slave_setup(0x40); // enable the slave w/ address 0x40
        
    CNCONbits.ON = 1;   // turn on CN bits
    CNENbits.CNEN8 = 1;    // CN8 as change notification pin10
    CNENbits.CNEN9 = 1;    // CN9 as change notification pin11
    CNENbits.CNEN10 = 1;    // CN10 as change notification pin12
    CNENbits.CNEN11 = 1;    // CN11 as change notification pin14
    IPC6bits.CNIP = 5;    // interrupt priority is 5 
    IPC6bits.CNIS = 1;    // sub-priority
    IFS1bits.CNIF = 0;    // clear interrupt flag
    IEC1bits.CNIE = 1;    // enable CN interrupt    
    __builtin_enable_interrupts(); // enable interrupts
    
    //Motor 1 PWM output
    T2CONbits.TCKPS = 2; // prescalar = 4
    PR2 = 199;  // period
    TMR2 = 0;   
    OC1CONbits.OCM = 0b110;
    OC1R = 0;
    OC1RS = 0; // start with 0 duty cycle
    T2CONbits.ON = 1; // turn on T2CON bits
    OC1CONbits.ON = 1; // turn on OC1CON bits
    // Motor 2 PWM output 
    T3CONbits.TCKPS = 2; // prescalar = 4
    PR3 = 199;  // period 
    TMR3 = 0;   
    OC3CONbits.OCTSEL = 1;  // use Timer3 for OC3
    OC3CONbits.OCM = 0b110;
    OC3R = 0;
    OC3RS = 0; // start with 0 duty cycle
    T3CONbits.ON = 1; // turn on T2CON bits
    OC3CONbits.ON = 1; // turn on OC4CON bits
    
    display_driver_write("yes man", 7); // print on lcd
    
    // Analog input parameters
    AD1PCFGbits.PCFG12 = 0; // set AN12 to input
    AD1CON3bits.ADCS = 2;  //  
    AD1CON1bits.ADON = 1;  // 
    
    while(1){
        isBlocked(); // get output from the distance sensor and determine if object is blocking the robot 
    } 

  return 0;  
}
