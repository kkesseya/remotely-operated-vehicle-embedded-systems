#include <xc.h> // PIC32 header file
#include <sys/attribs.h> // sys header file
#include <string.h> // header file to use strlen
#include <stdio.h>  // header file to use sprintf
#include "i2c_slave.h"
#include "display.h"
// Implements a I2C slave on I2C2 using pins SDA5 (F4) and SCL5 (F5), 58 and 59
// The slave returns the last two bytes the master writes
unsigned char byte;    // store two received bytes 
int wasBlocked = 0;    //status of if sensor was blocked

// I2C5 slave ISR
void __ISR(_I2C_5_VECTOR, IPL4SOFT) I2C5SlaveInterrupt(void) {
    // static unsigned char bytes[1]; // store two received bytes
    // We have to check why the interrupt occurred. Some possible causes:
    // (1) slave received its address with RW bit = 1: read address & send data to master
    // (2) slave received its address with RW bit = 0: read address (data will come next)
    // (3) slave received an ACK in RW = 1 mode: send data to master
    // (4) slave received a data byte in RW = 0 mode: store this data sent by master
    if (I2C5STATbits.D_A) { // received data/ACK, so Case (3) or (4)
        if (I2C5STATbits.R_W) { // Case (3): send data to master
            if(isBlocked() && !wasBlocked){
                I2C5TRN = 0x42; // load slave?s previously received data to send to master 
                wasBlocked = 1;
            }else if(!(isBlocked())){
                wasBlocked = 0;
            }
            I2C5CONbits.SCLREL = 1; // release the clock, allowing master to clock in data                           
        } else { // Case (4): we have received data from the master
            byte = I2C5RCV; // store the received data byte
            updateVehiclePath(byte);
        }
    } else { // the byte is an address byte, so Case (1) or (2)
        I2C5RCV; // read to clear I2C5RCV (we don?t need our own address)
        if (I2C5STATbits.R_W) { // Case (1): send data to master
            if(isBlocked() && !wasBlocked){
                I2C5TRN = 0x41; // load slave?s previously received data to send to master// load slave?s previously received data to send to master 
                wasBlocked = 1;
            }else if(!(isBlocked())){
                wasBlocked = 0;
            }
            I2C5CONbits.SCLREL = 1; // release the clock, allowing master to clock in data
        } // Case (2): do nothing more, wait for data to come
    }
    IFS1bits.I2C5SIF = 0;
}

// I2C5 slave setup (disable interrupts before calling)
void i2c_slave_setup(unsigned char addr) { 
    I2C5ADD = addr; // the address of the slave
    IPC8bits.I2C5IP = 4; // slave has interrupt priority 1
    IEC1bits.I2C5SIE = 1; // slave interrupt is enabled
    IFS1bits.I2C5SIF = 0; // clear the interrupt flag
    I2C5CONbits.ON = 1; // turn on i2c2
}
  
int isForward = 0, isBackward = 0, degreeX = 0, degreeY = 0;  // movement parameters

//turn left function
void moveLeft(){
    //motor 1
        LATDbits.LATD11 = 0; // m1in1
        LATDbits.LATD10 = 1; // m1in2
        OC1RS = 190;
    //motor 2
        LATBbits.LATB9 = 0; // m2in1
        LATBbits.LATB8 = 1; // m2in2
        OC3RS = 190;      
}

//turn right function
void moveRight(){
    //motor 1
        LATDbits.LATD11 = 1; // m1in1
        LATDbits.LATD10 = 0; // m1in2
        OC1RS = 190;
    //motor 2
        LATBbits.LATB9 = 1; // m2in1
        LATBbits.LATB8 = 0; // m2in2
        OC3RS = 190;  
}

//move forward function
void moveForward(){
    degreeX = getDegree1(); // get motor 1 angle
    degreeY = getDegree2(); // get motor 2 angle
    isForward = 1;
    //motor 1
        LATDbits.LATD11 = 1; //  m1in1
        LATDbits.LATD10 = 0; // m1in2
        OC1RS = control_1(degreeY, degreeX);
    //motor 2
        LATBbits.LATB9 = 0; // m2in1
        LATBbits.LATB8 = 1; // m2in2
        OC3RS = control_2(degreeX, degreeY);           
}

//move backward funtion
void moveBackward(){
    degreeX = getDegree1(); // get motor 1 angle
    degreeY = getDegree2(); // get motor 2 angle
    isBackward = 1;
    //motor 1
        LATDbits.LATD11 = 0; // m1in1
        LATDbits.LATD10 = 1; // m1in2
        OC1RS = control_1(degreeY, degreeX);
    //motor 2
        LATBbits.LATB9 = 1; // m2in1
        LATBbits.LATB8 = 0; // m2in2
        OC3RS = control_2(degreeX, degreeY);    
}

// stop moving function
void moveNull(){ 
        OC1RS = 0;
        OC3RS = 0; 
}

int getForward(){ // check if motor is moving forward
    return isForward;
}

int getBackward(){ // check if motor is moving backward
    return isBackward;
}

// Motor Control function
void updateVehiclePath(unsigned char pathCmd){
        if((pathCmd == 35) || (pathCmd == 36)){ //down(back) has been toggled
            if((pathCmd % 5) == 0){ //button was un pressed(released)
                LATA = 0b11000000; // indicate led for debug purpose
                moveNull(); // stop moving
                isBackward = 0; // store state that motor isn't moving backward
            }else{//button was pressed
                LATA = 0b00111111; // indicate led for debug purpose
                moveBackward(); // move backward
            }
        }else if((pathCmd == 25) || (pathCmd == 26)){ //right has been toggled
            if((pathCmd % 5) == 0){ //button was un pressed(released)
                LATA = 0b11110000; // indicate led for debug purpose
                moveNull(); // stop moving
            }else{//button was pressed
                LATA = 0b00001111; // indicate led for debug purpose
                moveRight(); // move right
            }
        }else if((pathCmd == 20) || (pathCmd == 21)){ //up(forward) has been toggled
            if((pathCmd % 5) == 0){ //button was un pressed(released)
                LATA = 0b11111100; // indicate led for debug purpose
                moveNull(); // stop moving
                isForward = 0; // store state that motor isn't moving forward
            }else{//button was pressed
                if(!(isBlocked())){ // if distance sensor is not blocked, move forward
                    moveForward(); 
                    LATA = 0b00000011; // indicate led for debug purpose
                }     
            }
        }else if((pathCmd == 30) || (pathCmd == 31)){ //left has been toggled
            if((pathCmd % 5) == 0){ //button was un pressed(released)
                LATA = 0b10101111; // indicate led for debug purpose
                moveNull(); // stop moving
            }else{//button was pressed
                LATA = 0b01010000; // indicate led for debug purpose
                moveLeft(); // move left
            }
        }else if((pathCmd == 40) || (pathCmd == 41)){ //r1(trigger) has been toggled
            if((pathCmd % 5) == 0){ //button was un pressed(released)
                LATA = 0b01010101; // indicate led for debug purpose
            }else{//button was pressed
                LATA = 0b10101010; // indicate led for debug purpose
            }
        }else if((pathCmd == 45) || (pathCmd == 46)){ //l1(trigger) has been toggled
            if((pathCmd % 5) == 0){ //button was un pressed(released)
                LATA = 0b01010101; // indicate led for debug purpose
            }else{//button was pressed
                LATA = 0b10101010; // indicate led for debug purpose
            }
        }  
    return;
}