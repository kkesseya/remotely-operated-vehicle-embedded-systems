#ifndef LCD_DISPLAY
#define LCD_DISPLAY

void display_driver_enable();

void display_driver_delay();

void display_driver_initialize();

void display_driver_clear();

void display_driver_write(char *data, int length);

int isBlocked();

#endif
