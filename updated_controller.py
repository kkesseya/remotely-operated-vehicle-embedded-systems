#!/usr/bin/env python
#include all necessary libraries
from turtle import Turtle
import time
import smbus #library used for I2C communication Protocol
from inputs import devices
from inputs import get_gamepad
t1 = Turtle()#get an instance of a graphics class
#boolean conditions
forward = 0
back = 0
left = 0
right = 0

def encode(str_code, state):#encoder takes in a msg and the state from the controller and encodes it as a byte value
	if(str_code == 'BTN_WEST'):
		forward = state
		return 0x14
	elif(str_code == 'BTN_EAST'):
		right = state
		return 0x19
	elif(str_code == 'BTN_NORTH'):
		left = state
		return 0x1E
	elif(str_code == 'BTN_SOUTH'):
		back = state
		return 0x23
	elif(str_code == 'BTN_TR'):
		return 0x28
	elif(str_code == 'BTN_TL'):
		return 0x2D
	else:
		return 0
	
bus = smbus.SMBus(1) #set up smbus used for i2c communication
i2c_address = 0x40 #pic32 address

gamepad = devices.gamepads[0]
#setting up graphics
ts = t1.getscreen()
ts.setup(width=1.0, height=1.0, startx=None, starty=None)#screen resize

t1.shape("arrow")#shape
t1.shapesize(15,15,1)


while True:
	gamepad_events = gamepad.read()#wait for a value from the controller 
	for gamepad_event in gamepad_events:
		if 'BTN_' in  gamepad_event.code:
			msg = encode(gamepad_event.code, gamepad_event.state)+gamepad_event.state #send values from controller into encoder
			print(gamepad_event.code)
			if (msg == 21): #forward graphics
				t1.setheading(90)
				ts.bgcolor("blue")
			if (msg == 31): #left graphics
				t1.setheading(180)
				ts.bgcolor("orange")
			if (msg == 26): #right graphics
				t1.setheading(0)
				ts.bgcolor("red")
			if (msg == 36): #back graphics
				t1.setheading(270)
				ts.bgcolor("green")
			bus.write_byte(i2c_address,msg) #write or send the controller value to the pic on the i2c bus
			print(msg)
			break

